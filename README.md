# Custom Navigate Flow Action (LEX ONLY - Does not work in Classic)
### This custom flow action allows you to:  
	-  Navigate a user to a Record�in LEX 
		-   Simply pass the record Id into the flow action  
		-   Helpful if you need to 'refresh' the current lightning record page after completion of a flow so you can see updated record values on the page  
		-   Allows you to finish your lightning flow by navigating a user to a specific record, like one that you dynamically created in the flow  
		
	-  Navigate a user to a specific URL  
		-   URLs can be relative urls, such as navigating the user to the Home tab, for example�'/lightning/page/home'  
		-   URLS can also be absolute, which can navigate a user to an external link (opens in a new tab so you don't leave Salesforce)  

## Attributes
### NewTab
If checked, navigation will open a new browser tab
### RecordId
Record ID of the record to which you wish to redirect the user
### URL
URL to which you wish to redirect the user.  Can be relative, e.g. '/someotherdirectory/somefile.txt', or absolute, e.g. 'https://www.google.com'

## Implementation
This lightning component can be added to any Lightning Flow from the Core Actions menu in the flow builder.  Once you add the Custom Navigate Flow Action element to your flow, simply specify the Record Id or the URL to which you wish to navigate the user.  This can be very helpful in dynamic user screen flow when creating records on the fly and you wish to finish the flow by navigating the user to the new record.