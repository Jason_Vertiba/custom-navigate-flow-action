({
	navigateToRecord : function(component, event, recordId) {
        let pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: recordId,
                actionName: "view"
            }
        };
        this.navigateHelper(component, event, pageReference);
	},
    
    navigateToURL : function(component, event, url) {
        let pageReference = {
            type: 'standard__webPage',
            attributes: {
                url: url
            }
        };
        this.navigateHelper(component, event, pageReference);
	},
    
    navigateHelper : function (component, event, pageReference){
        const newTab = component.get('v.newTab');
        let navService = component.find("navService");
        if(newTab){
            //  To open on a new tab, generate the URL manually and use window.open
            navService.generateUrl(pageReference)
            .then(function(url){
                window.open(url, '_blank');
            })
            .catch(function(error){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Navigation Failed!",
                    "type": "error",
                    "message": "Something went wrong generating the URL. ERROR: " + error
                });
                toastEvent.fire();
            });
        } else {
            //  Navigate to the page reference
            navService.navigate(pageReference,false);
        }
        
    },
    
})