({
	invoke : function(component, event, helper) {
        // Get the record ID attribute
        const recordId = component.get("v.recordId");
        const url = component.get("v.url");
        
        // Check if redirecting to a record or a URL
        if(!$A.util.isEmpty(recordId)){
            helper.navigateToRecord(component, event, recordId);
            
        } else if(!$A.util.isEmpty(url)) {
            helper.navigateToURL(component, event, url);
            
        } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Navigation Failed!",
                "type": "error",
                "message": "Record Id or URL not specified."
            });
            toastEvent.fire();
        }
        
        
    },
})